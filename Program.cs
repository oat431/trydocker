﻿using System;
using System.Threading.Tasks;

namespace Flowero
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var counter = 0;
            while(true)
            {
                Console.WriteLine($"Counter : {++counter}");
                await Task.Delay(1000);
            }
        }
    }
}
